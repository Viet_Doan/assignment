function RobotMove (robot,x_,y_,z_)
steps = 50;
workspace = [-0.5 0.5 -0.5 0.5 -0.5 0.5];
currentPos = [robot.getpos];
T1 = robot.fkine(currentPos);
T1 = T1(1:3,4);
desiredPos = [DobotIkine(robot,x_,y_,z_)];
T2 = robot.fkine(desiredPos);

deltaT = 0.05;                                        % Discrete time step
x = nan(3,steps);
%x = zeros(robot.n,steps);
s = lspb(0,1,steps);                                 % Create interpolation scalar
for i = 1:steps
    x(:,i) = T1*(1-s(i)) + s(i)*[x_ y_ z_]';

end
plot3(x(1,:),x(2,:),x(3,:),'k.','LineWidth',1);

qMatrix = nan(steps,robot.n);

% 3.9
qMatrix(1,:) = currentPos;

% 3.10
for i = 1:steps-1
    xdot = (x(:,i+1) - x(:,i))/deltaT;                             % Calculate velocity at discrete time step
    J = robot.jacob0(qMatrix(i,:));            % Get the Jacobian at the current state
    J = J(1:3,1:3);                           
    qdot = inv(J)*xdot;                             % Solve velocitities via RMRC
    qMatrix(i+1,:) =  qMatrix(i,:) + deltaT*[qdot' 0 0];            
  
end
for i = 1:steps
    qMatrix(i,4) = pi/2 - qMatrix(i,3) - qMatrix(i,2);
end


robot.plot3d(qMatrix,'workspace',workspace);

end
