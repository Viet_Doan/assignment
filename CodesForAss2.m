
clear all
clc
clf

%% setting DH parameters


L1 = Link('d',0.05,'a',0,'alpha',-pi/2,'offset',0,'qlim',[deg2rad(-135),deg2rad(135)]) %joint 1 

L2 = Link('d',0,'a',0.135,'alpha',0,'offset',-pi/2,'qlim',[deg2rad(5),deg2rad(80)]) %joint 2

L3 = Link('d',0,'a',0.160,'alpha',0,'offset',0,'qlim',[deg2rad(15),deg2rad(170)]) %joint 3

L4 = Link('d',0,'a',0.05,'alpha',-pi/2, 'offset',0, 'qlim',[-pi/2,pi/2]) %joint 4

L5 = Link('d',0.05,'a',0,'alpha',0,'offset',0 ,'qlim',[deg2rad(-85),deg2rad(85)]) %joint 5

myRobot1 = SerialLink([L1 L2 L3 L4 L5], 'name', 'Dobot');
for linkIndex = 0:myRobot1.n      %% arm 1
    [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['Link',num2str(linkIndex),'.ply'],'tri'); %%use plyread to get the 3d models
    myRobot1.faces{linkIndex+1} = faceData;   %assign corresponding faces and vertices 
    myRobot1.points{linkIndex+1} = vertexData;
end

q = zeros(1,5)

workspace = [-0.5 0.5 -0.5 0.5 -0.5 0.5];


qr=DobotIkine(myRobot1,0.15,0.1,0.03);


myRobot1.plot3d(qr,'workspace',workspace);


%myRobot1.teach
RobotMove(myRobot1,-0.15,0.1,-0.08);
