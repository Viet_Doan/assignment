function RobotMoveJoystick (robot)
steps = 50;
workspace = [-0.5 0.5 -0.5 0.5 -0.5 0.5];
currentPos = [robot.getpos];
T1 = robot.fkine(currentPos);
T1 = T1(1:3,4);
id = 1;
joy = vrjoystick(id);
caps(joy) % display joystick information
qMatrix(1,:) = currentPos;
deltaT = 0.05;     
while(pi>1)

    % read joystick
    [axes, buttons, povs] = read(joy);

    K1 = 0.3;
    vx = K1*axes(2);
    vy = K1*axes(1);
    vz = K1*axes(6);
    wx =0;
    wy =0;
    wz =0;


    if (abs(vx)<0.02 && abs(vy)<0.02 && abs(vz)<0.02 )

    else
    xdot = [vx vy vz]'
    
%     qMatrix = zeros(steps,6);
    J = robot.jacob0(qMatrix);            % Get the Jacobian at the current state
    J = J(1:3,1:3);                           % Take only first 2 rows
    qdot = inv(J)*xdot;                             % Solve velocitities via RMRC
    qMatrix =  qMatrix + deltaT*[qdot' 0 0];
    qMatrix(1,4) = pi/2 - qMatrix(1,3) - qMatrix(1,2);

    end

    robot.animate(qMatrix);  
    
    % wait until loop time elapsed

end
